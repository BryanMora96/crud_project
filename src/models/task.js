//Contentiene el esquema que de la base de datos
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//contiene cada uno de los campso que seran almacenados en la base de datos
const TaskSchema = new Schema({
    tipoevento: String,
    fechaevento: String,
    rangoedad: String, 
    direccion: String,
    description: String,
    status: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('tasks', TaskSchema);